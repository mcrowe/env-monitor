# Setup
- Use the recommended node version using nvm: `nvm use`
- Install required node modules: `npm install`
- Download Arduino IDE
- Plug in your Arduino or Arduino compatible microcontroller via USB
- Open the Arduino IDE, select: File > Examples > Firmata > StandardFirmata
- Click the "Upload" button.

# Usage
```
node monitor.js
```

# Running as a background service

See http://goo.gl/nw89ka for a good description of the options.

Forever will run in the background, redirect stdout to a log, and restart the app if it crashes.

Install

```
npm install -g forever
```

Start

```
forever start monitor.js
```

Manage

```
forever list
forever stop 0
...
```
var request = require('request'),
    five = require('johnny-five'),
    board = new five.Board()

var PIN = 0

var count = 0
var sum = 0

var READINGS_URL = 'http://glacial-sands-5442.herokuapp.com/readings'
// var READINGS_URL = 'http://localhost:3000/readings'
var INTEGRATION_FREQUENCY = 60000 // Integrate every 1-minute.

var getAverage = function() {
  if (count > 0) {
    return sum / count
  } else {
    return null
  }
}

var record = function(value) {
  var voltage = value * 5 / 1024,
      temperature = (voltage - 0.5) * 100

  count += 1
  sum += temperature
}

var reset = function() {
  count = 0
  sum = 0
}


var integrate = function () {
  if (count == 0) { return; }

  var avg = sum / count
  var time = (new Date()).getTime()

  request.post(READINGS_URL, {form: { value: avg, timestamp: time }})

  console.log(time + ': ' + avg)

  count = 0
  sum = 0
}

board.on('ready', function() {

  console.log('\nCONNECTED to arduino')

  setInterval(integrate, INTEGRATION_FREQUENCY)

  this.pinMode(PIN, five.Pin.ANALOG)
  this.analogRead(PIN, record)

})